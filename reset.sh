#!/bin/bash

docker-compose down
sudo rm -rf volumes
docker-compose up -d
eval "$(./bashrc_generate.sh | tail -n 6)"
./run_create_bucket.sh
./quickstart/mlflow_tracking.py
